﻿namespace Burda
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.tbxT = new System.Windows.Forms.TextBox();
            this.tbxC = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxB = new System.Windows.Forms.CheckBox();
            this.cbxA = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // tbxT
            // 
            this.tbxT.Location = new System.Drawing.Point(12, 12);
            this.tbxT.Name = "tbxT";
            this.tbxT.Size = new System.Drawing.Size(100, 20);
            this.tbxT.TabIndex = 1;
            // 
            // tbxC
            // 
            this.tbxC.Location = new System.Drawing.Point(12, 67);
            this.tbxC.Name = "tbxC";
            this.tbxC.Size = new System.Drawing.Size(29, 20);
            this.tbxC.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter number of characters in 2nd textbox!";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // cbxB
            // 
            this.cbxB.AutoSize = true;
            this.cbxB.Location = new System.Drawing.Point(118, 44);
            this.cbxB.Name = "cbxB";
            this.cbxB.Size = new System.Drawing.Size(101, 17);
            this.cbxB.TabIndex = 4;
            this.cbxB.Text = "Include symbols";
            this.cbxB.UseVisualStyleBackColor = true;
            // 
            // cbxA
            // 
            this.cbxA.AutoSize = true;
            this.cbxA.Location = new System.Drawing.Point(118, 15);
            this.cbxA.Name = "cbxA";
            this.cbxA.Size = new System.Drawing.Size(104, 17);
            this.cbxA.TabIndex = 5;
            this.cbxA.Text = "Include numbers";
            this.cbxA.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 120);
            this.Controls.Add(this.cbxA);
            this.Controls.Add(this.cbxB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxC);
            this.Controls.Add(this.tbxT);
            this.Controls.Add(this.button1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbxT;
        private System.Windows.Forms.TextBox tbxC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbxB;
        private System.Windows.Forms.CheckBox cbxA;
    }
}

