﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Burda
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

      public void generateText()
            {
            //exception handling
            int a = 0;
            if (String.IsNullOrEmpty(tbxC.Text) || String.IsNullOrWhiteSpace(tbxC.Text)) return;
            if (int.TryParse(tbxC.Text, out a) == false) return;
            int n = Int32.Parse(tbxC.Text);
            if (n > 12 || n < 0) return;//a little limit for your password generator                             
            int[] int_msg = {0x30,0x31,0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,//numbers
                0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,0x71,//lower case letters
                0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70,
                0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 
                0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,//upper case letters
                0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,
                0x21,0x22,0x23,0x24,0x24,0x25,0x26,0x27,0x27,0x28,0x29,0x2a,//symbols
                0x2b,0x2c,0x2d,0x2e,0x2f,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x5b,0x5c,
                0x5d,0x5f,0x60
                };
            //generating password
            Random rand = new Random();     
            byte[] new_msg = new byte[n];
            for (int i = 0; i < n; i++){                            
                new_msg[i] = Convert.ToByte(int_msg[rand.Next(11, 47)]);              
            }
            if(cbxA.Checked == true){
                for (int i = 0; i < n; i++){
                    new_msg[i] = Convert.ToByte(int_msg[rand.Next(0, 47)]);
                }
            }
            if (cbxB.Checked == true){
                for (int i = 0; i < n; i++){
                    new_msg[i] = Convert.ToByte(int_msg[rand.Next(11, 76)]);
                }
            }
            if(cbxA.Checked == true || cbxB.Checked == true){
                for (int i = 0; i < n; i++){
                    new_msg[i] = Convert.ToByte(int_msg[rand.Next(0, 76)]);
                }
            }
            string msg = Encoding.UTF8.GetString(new_msg);
            tbxT.Text = msg;
            }
        private void Button1_Click(object sender, EventArgs e)
        {
            generateText();
            
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
